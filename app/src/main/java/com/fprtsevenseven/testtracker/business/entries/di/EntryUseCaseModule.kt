package com.fprtsevenseven.testtracker.business.entries.di

import com.fprtsevenseven.testtracker.business.entries.usecases.*
import com.fprtsevenseven.testtracker.data.entries.repositories.DatabaseRepository
import com.fprtsevenseven.testtracker.data.entries.repositories.RemoteRepository
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ViewModelComponent

@InstallIn(ViewModelComponent::class)
@Module
object EntryUseCaseModule {

    @Provides
    fun entryListUseCase(
        databaseRepository: DatabaseRepository,
        remoteRepository: RemoteRepository
    ): EntryListUseCase = EntryListUseCaseImpl(databaseRepository, remoteRepository)

    @Provides
    fun entryDetailUseCase(
        databaseRepository: DatabaseRepository,
        remoteRepository: RemoteRepository
    ): EntryDetailUseCase = EntryDetailUseCaseImpl(databaseRepository, remoteRepository)

    @Provides
    fun entryCreateUseCase(
        databaseRepository: DatabaseRepository,
        remoteRepository: RemoteRepository
    ): EntryCreateUseCase = EntryCreateUseCaseImpl(databaseRepository, remoteRepository)

}
