package com.fprtsevenseven.testtracker.business.entries.usecases

import com.fprtsevenseven.testtracker.business.entries.face.EntryFace
import com.fprtsevenseven.testtracker.data.entries.repositories.DatabaseRepository
import com.fprtsevenseven.testtracker.data.entries.repositories.RemoteRepository
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.zip

interface EntryListUseCase {

    fun getEntries(): Flow<List<EntryFace>>
}

class EntryListUseCaseImpl(
    private val databaseRepository: DatabaseRepository,
    private val remoteRepository: RemoteRepository,
) : EntryListUseCase {

    override fun getEntries(): Flow<List<EntryFace>> {
        return databaseRepository.getEntries()
            .zip(remoteRepository.getEntries()) { databaseData, remoteData ->
                val size1 = databaseData.size
                val size2 = remoteData.size
                databaseData.toMutableList().apply { addAll(remoteData) }.sortedBy { it.name }
            }
    }

}
