package com.fprtsevenseven.testtracker.business.entries.face

import java.io.Serializable

data class EntryDetailFace(
    val entryFace: EntryFace,
    val timestamp: String,
    val coordinates: String,
) : Serializable
