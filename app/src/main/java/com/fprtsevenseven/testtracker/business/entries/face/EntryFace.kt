package com.fprtsevenseven.testtracker.business.entries.face

import androidx.recyclerview.widget.DiffUtil.ItemCallback
import com.fprtsevenseven.testtracker.data.EntrySource
import java.io.Serializable

data class EntryFace(
    val id: String,
    val name: String,
    val place: String,
    val duration: String,
    val source: EntrySource,
) : Serializable {
    companion object {
        val diffCallback = object : ItemCallback<EntryFace>() {
            override fun areItemsTheSame(p0: EntryFace, p1: EntryFace): Boolean =
                p0.id == p1.id && p0.source == p1.source

            override fun areContentsTheSame(p0: EntryFace, p1: EntryFace): Boolean = p0 == p1
        }
    }
}
