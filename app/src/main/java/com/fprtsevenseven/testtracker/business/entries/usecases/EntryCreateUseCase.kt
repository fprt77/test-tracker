package com.fprtsevenseven.testtracker.business.entries.usecases

import com.fprtsevenseven.testtracker.business.entries.face.EntryDetailFace
import com.fprtsevenseven.testtracker.business.entries.face.EntryFace
import com.fprtsevenseven.testtracker.data.EntrySource
import com.fprtsevenseven.testtracker.data.entries.repositories.DatabaseRepository
import com.fprtsevenseven.testtracker.data.entries.repositories.RemoteRepository
import kotlinx.coroutines.flow.Flow

interface EntryCreateUseCase {

    fun createEntry(data: EntryInputData): Flow<Boolean>

    data class EntryInputData(
        val name: String,
        val place: String,
        val duration: String,
        val timestamp: String,
        val coordinates: String,
        val source: EntrySource,
    )
}

class EntryCreateUseCaseImpl(
    private val databaseRepository: DatabaseRepository,
    private val remoteRepository: RemoteRepository,
) : EntryCreateUseCase {

    override fun createEntry(data: EntryCreateUseCase.EntryInputData): Flow<Boolean> {
        val face = EntryDetailFace(
            entryFace = EntryFace(
                id = System.nanoTime().toString(), // just easy way to generate unique ID
                name = data.name,
                place = data.place,
                duration = data.duration,
                source = data.source,
            ),
            timestamp = data.timestamp,
            coordinates = data.coordinates,
        )
        return when (face.entryFace.source) {
            EntrySource.DATABASE -> databaseRepository.createEntry(face)
            EntrySource.REMOTE -> remoteRepository.createEntry(face)
        }
    }

}
