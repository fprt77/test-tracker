package com.fprtsevenseven.testtracker.business.entries.usecases

import com.fprtsevenseven.testtracker.business.entries.face.EntryDetailFace
import com.fprtsevenseven.testtracker.business.entries.face.EntryFace
import com.fprtsevenseven.testtracker.data.EntrySource
import com.fprtsevenseven.testtracker.data.entries.repositories.DatabaseRepository
import com.fprtsevenseven.testtracker.data.entries.repositories.RemoteRepository
import kotlinx.coroutines.flow.Flow

interface EntryDetailUseCase {

    fun getEntry(face: EntryFace): Flow<EntryDetailFace?>

    fun removeEntry(face: EntryDetailFace): Flow<Boolean>
}

class EntryDetailUseCaseImpl(
    private val databaseRepository: DatabaseRepository,
    private val remoteRepository: RemoteRepository,
) : EntryDetailUseCase {

    override fun getEntry(face: EntryFace): Flow<EntryDetailFace?> {
        return when (face.source) {
            EntrySource.DATABASE -> databaseRepository.getEntry(face)
            EntrySource.REMOTE -> remoteRepository.getEntry(face)
        }
    }

    override fun removeEntry(face: EntryDetailFace): Flow<Boolean> {
        return when (face.entryFace.source) {
            EntrySource.DATABASE -> databaseRepository.removeEntry(face)
            EntrySource.REMOTE -> remoteRepository.removeEntry(face)
        }
    }

}
