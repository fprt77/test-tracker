package com.fprtsevenseven.testtracker.presentation

import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.isGone
import androidx.core.view.isVisible
import com.fprtsevenseven.testtracker.R
import com.fprtsevenseven.testtracker.databinding.ActivityMainBinding
import com.fprtsevenseven.testtracker.presentation.entries.fragments.BottomSheetCreateFragment
import com.fprtsevenseven.testtracker.presentation.entries.viewmodels.EntryDetailVM
import com.fprtsevenseven.testtracker.presentation.entries.viewmodels.EntryListVM
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class EntriesActivity : AppCompatActivity() {

    private val listVM by viewModels<EntryListVM>()
    private val detailVM by viewModels<EntryDetailVM>()

    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        setObservers()
    }

    private fun setObservers() {
        listVM.selectedEntry.observe(this) {
            if (it != null) {
                binding.detail.isVisible = true
                detailVM.loadEntryDetail(it)
            } else {
                binding.detail.isGone = true
            }
        }
        detailVM.closeRequested.observe(this) {
            listVM.closeDetail()
        }
        detailVM.dataChanged.observe(this) {
            listVM.loadEntries()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.entries_menu, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.create -> {
                // open create fragment
                BottomSheetCreateFragment().show(supportFragmentManager, BOTTOM_SHEET_TAG)
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    companion object {
        private const val BOTTOM_SHEET_TAG = "BOTTOM_SHEET_TAG"
    }
}