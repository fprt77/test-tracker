package com.fprtsevenseven.testtracker.presentation.entries.viewmodels

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.fprtsevenseven.testtracker.business.entries.usecases.EntryCreateUseCase
import com.fprtsevenseven.testtracker.data.base.ActionLiveData
import com.fprtsevenseven.testtracker.data.base.NetworkState
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class EntryCreateVM @Inject constructor(
    private val entryCreateUseCase: EntryCreateUseCase
) : ViewModel() {
    val loadingState = MutableLiveData<NetworkState>()
    val closeRequested = ActionLiveData<Boolean>()

    fun loadEntryDetail(face: EntryCreateUseCase.EntryInputData) {
        loadingState.value = NetworkState.Loading
        viewModelScope.launch {
            entryCreateUseCase.createEntry(face).collect {
                loadingState.value = NetworkState.Loaded
                if (it) {
                    closeRequested.sendAction(true)
                }
            }
        }
    }
}
