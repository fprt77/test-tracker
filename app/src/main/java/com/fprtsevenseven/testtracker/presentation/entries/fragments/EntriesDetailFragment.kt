package com.fprtsevenseven.testtracker.presentation.entries.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import androidx.core.view.isGone
import androidx.core.view.isVisible
import androidx.fragment.app.activityViewModels
import com.fprtsevenseven.testtracker.data.base.NetworkState
import com.fprtsevenseven.testtracker.databinding.FEntryDetailBinding
import com.fprtsevenseven.testtracker.presentation.base.fragments.BaseFragment
import com.fprtsevenseven.testtracker.presentation.entries.viewmodels.EntryDetailVM

class EntriesDetailFragment : BaseFragment<FEntryDetailBinding>() {

    private val vm by activityViewModels<EntryDetailVM>()

    override fun inflateBinding(inflater: LayoutInflater) =
        FEntryDetailBinding.inflate(layoutInflater)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.close.setOnClickListener {
            vm.closeDetail()
        }
        binding.actionDelete.setOnClickListener {
            vm.delete()
        }

        setObservers()
    }

    override fun setObservers() {
        vm.entryDetail.observe(viewLifecycleOwner) {
            binding.name.text = it.entryFace.name
            binding.place.text = it.entryFace.place
            binding.duration.text = it.entryFace.duration
            binding.timestamp.text = it.timestamp
            binding.coordinates.text = it.coordinates
        }
        vm.loadingState.observe(viewLifecycleOwner) {
            when (it) {
                NetworkState.NoData,
                is NetworkState.Error,
                NetworkState.Loaded -> binding.progress.progress.isGone = true
                NetworkState.Loading -> binding.progress.progress.isVisible = true
            }
        }
    }
}
