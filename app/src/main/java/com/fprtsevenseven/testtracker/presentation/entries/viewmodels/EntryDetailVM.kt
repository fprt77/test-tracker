package com.fprtsevenseven.testtracker.presentation.entries.viewmodels

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.fprtsevenseven.testtracker.business.entries.face.EntryDetailFace
import com.fprtsevenseven.testtracker.business.entries.face.EntryFace
import com.fprtsevenseven.testtracker.business.entries.usecases.EntryDetailUseCase
import com.fprtsevenseven.testtracker.data.base.ActionLiveData
import com.fprtsevenseven.testtracker.data.base.NetworkState
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class EntryDetailVM @Inject constructor(
    private val entryDetailUseCase: EntryDetailUseCase
) : ViewModel() {
    val loadingState = MutableLiveData<NetworkState>()
    val entryDetail = MutableLiveData<EntryDetailFace>()
    val dataChanged = ActionLiveData<Unit>()
    val closeRequested = ActionLiveData<Unit>()

    fun loadEntryDetail(face: EntryFace) {
        loadingState.value = NetworkState.Loading
        viewModelScope.launch {
            entryDetailUseCase.getEntry(face).collect {
                entryDetail.value = it
                if (it == null) {
                    loadingState.value = NetworkState.NoData
                } else {
                    loadingState.value = NetworkState.Loaded
                }
            }
        }
    }

    fun closeDetail() {
        closeRequested.sendAction(Unit)
    }

    fun delete() {
        entryDetail.value?.let { face ->
            viewModelScope.launch {
                entryDetailUseCase.removeEntry(face).collect {
                    if (it) {
                        dataChanged.sendAction(Unit)
                        closeDetail()
                    }
                }
            }
        }
    }
}
