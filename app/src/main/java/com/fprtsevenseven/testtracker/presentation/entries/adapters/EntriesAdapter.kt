package com.fprtsevenseven.testtracker.presentation.entries.adapters

import android.content.res.ColorStateList
import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.content.res.AppCompatResources
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.fprtsevenseven.testtracker.R
import com.fprtsevenseven.testtracker.business.entries.face.EntryFace
import com.fprtsevenseven.testtracker.data.EntrySource
import com.fprtsevenseven.testtracker.databinding.LEntryItemBinding

class EntriesAdapter(
    private val onItemClick: (EntryFace) -> Unit
) : ListAdapter<EntryFace, EntriesAdapter.ViewHolder>(EntryFace.diffCallback) {

    private val itemLayout = R.layout.l_entry_item

    override fun onAttachedToRecyclerView(recyclerView: RecyclerView) {
        super.onAttachedToRecyclerView(recyclerView)
        AppCompatResources.getDrawable(recyclerView.context, R.drawable.list_divider)
            ?.let { divider ->
                val orientation = (recyclerView.layoutManager as? LinearLayoutManager)?.orientation
                if (orientation != null) {
                    recyclerView.addItemDecoration(
                        DividerItemDecoration(recyclerView.context, orientation).apply {
                            setDrawable(divider)
                        }
                    )
                }
            }
    }

    private fun onBindView(item: EntryFace, itemView: View, position: Int) {
        val binding = LEntryItemBinding.bind(itemView)
        binding.topLine.text = item.name
        binding.bottomLine.text = "${item.place}, ${item.duration}"
        binding.source.text = when (item.source) {
            EntrySource.DATABASE -> "D"
            EntrySource.REMOTE -> "R"
        }
        binding.source.setTextColor(
            when (item.source) {
                EntrySource.DATABASE -> ColorStateList.valueOf(Color.GREEN)
                EntrySource.REMOTE -> ColorStateList.valueOf(Color.RED)
            }
        )
        binding.root.setOnClickListener { onItemClick(item) }
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(position)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(parent.context).inflate(itemLayout, parent, false))
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        private var item: EntryFace? = null

        fun bind(position: Int) {
            item = getItem(position)
            item?.let { onBindView(it, itemView, position) }
        }
    }
}
