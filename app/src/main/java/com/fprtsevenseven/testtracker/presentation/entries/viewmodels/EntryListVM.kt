package com.fprtsevenseven.testtracker.presentation.entries.viewmodels

import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.fprtsevenseven.testtracker.business.entries.face.EntryFace
import com.fprtsevenseven.testtracker.business.entries.usecases.EntryListUseCase
import com.fprtsevenseven.testtracker.data.EntrySource
import com.fprtsevenseven.testtracker.data.base.NetworkState
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class EntryListVM @Inject constructor(
    private val entryListUseCase: EntryListUseCase,
) : ViewModel() {
    val loadingState = MutableLiveData<NetworkState>()
    private val filterSource = MutableLiveData<EntrySource?>()
    private val allEntries = MutableLiveData<List<EntryFace>>()
    val selectedEntry = MutableLiveData<EntryFace?>()
    val entries = MediatorLiveData<List<EntryFace>>().also {
        it.addSource(filterSource) {
            updateEntryList()
        }
        it.addSource(allEntries) {
            updateEntryList()
        }
    }

    fun loadEntries() {
        loadingState.value = NetworkState.Loading
        viewModelScope.launch {
            entryListUseCase.getEntries().collect {
                allEntries.value = it
                if (it.isEmpty()) {
                    loadingState.value = NetworkState.NoData
                } else {
                    loadingState.value = NetworkState.Loaded
                }
            }
        }
    }

    fun openDetail(entryFace: EntryFace) {
        selectedEntry.value = entryFace
    }

    fun closeDetail() {
        selectedEntry.value = null
    }

    fun filter(source: EntrySource?) {
        filterSource.value = source
    }

    private fun updateEntryList() {
        val source = filterSource.value
        entries.value =
            if (source != null) allEntries.value?.filter { it.source == source } else allEntries.value
    }
}
