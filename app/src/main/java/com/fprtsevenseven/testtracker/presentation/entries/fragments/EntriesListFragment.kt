package com.fprtsevenseven.testtracker.presentation.entries.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import androidx.core.view.children
import androidx.core.view.isGone
import androidx.core.view.isVisible
import androidx.fragment.app.activityViewModels
import com.fprtsevenseven.testtracker.R
import com.fprtsevenseven.testtracker.data.EntrySource
import com.fprtsevenseven.testtracker.data.base.NetworkState
import com.fprtsevenseven.testtracker.databinding.FEntryListBinding
import com.fprtsevenseven.testtracker.presentation.base.fragments.BaseFragment
import com.fprtsevenseven.testtracker.presentation.entries.adapters.EntriesAdapter
import com.fprtsevenseven.testtracker.presentation.entries.viewmodels.EntryListVM
import com.google.android.material.chip.Chip
import com.google.android.material.chip.ChipGroup

class EntriesListFragment : BaseFragment<FEntryListBinding>() {

    private val vm by activityViewModels<EntryListVM>()

    override fun inflateBinding(inflater: LayoutInflater) =
        FEntryListBinding.inflate(layoutInflater)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.chipGroup.isSingleSelection = true
        binding.chipGroup.setup()
        binding.swipeRefresh.setOnRefreshListener { vm.loadEntries() }

        val adapter = EntriesAdapter { face ->
            vm.openDetail(face)
        }
        binding.recyclerView.adapter = adapter

        setObservers()
        vm.loadEntries()
    }

    override fun setObservers() {
        vm.entries.observe(viewLifecycleOwner) {
            val adapter = binding.recyclerView.adapter as? EntriesAdapter
            adapter?.submitList(it)
        }
        vm.loadingState.observe(viewLifecycleOwner) {
            when (it) {
                NetworkState.NoData,
                is NetworkState.Error,
                NetworkState.Loaded -> {
                    binding.swipeRefresh.isRefreshing = false
                    binding.progress.progress.isGone = true
                }
                NetworkState.Loading -> binding.progress.progress.isVisible = true
            }
        }
    }

    private fun ChipGroup.setup() {
        children.forEachIndexed { index, it ->
            val chip = (it as Chip)
            chip.setOnCheckedChangeListener { button, checked ->
                if (checked) {
                    when (button.id) {
                        R.id.chipAll -> vm.filter(null)
                        R.id.chipDatabase -> vm.filter(EntrySource.DATABASE)
                        R.id.chipRemote -> vm.filter(EntrySource.REMOTE)
                    }
                }
            }
            chip.isCheckable = true // otherwise it cannot be changed
            chip.isChecked = index == 0
        }
    }
}
