package com.fprtsevenseven.testtracker.presentation.entries.fragments

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import androidx.core.view.isGone
import androidx.core.view.isVisible
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import com.fprtsevenseven.testtracker.R
import com.fprtsevenseven.testtracker.business.entries.usecases.EntryCreateUseCase
import com.fprtsevenseven.testtracker.data.EntrySource
import com.fprtsevenseven.testtracker.data.base.NetworkState
import com.fprtsevenseven.testtracker.databinding.FEntryCreateBinding
import com.fprtsevenseven.testtracker.presentation.entries.viewmodels.EntryCreateVM
import com.fprtsevenseven.testtracker.presentation.entries.viewmodels.EntryDetailVM
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.google.android.material.textfield.TextInputLayout
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class BottomSheetCreateFragment : BottomSheetDialogFragment() {

    private lateinit var binding: FEntryCreateBinding

    private val vm by viewModels<EntryCreateVM>()
    private val activityVM by activityViewModels<EntryDetailVM>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(DialogFragment.STYLE_NORMAL, R.style.BottomSheetDialogThemeNoFloating)
    }

    override fun onStart() {
        super.onStart()
        //this forces the sheet to appear at max height even on landscape
        val behavior = BottomSheetBehavior.from(requireView().parent as View)
        behavior.state = BottomSheetBehavior.STATE_EXPANDED
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FEntryCreateBinding.inflate(inflater, container, false)

        binding.name.addErrorClearingTextChanged(binding.tilName)
        binding.place.addErrorClearingTextChanged(binding.tilPlace)
        binding.duration.addErrorClearingTextChanged(binding.tilDuration)
        binding.radioGroup.check(binding.rbDatabase.id)
        binding.actionSubmit.setOnClickListener { submitForm() }
        binding.close.setOnClickListener { dismiss() }

        setObservers()

        return binding.root
    }

    private fun setObservers() {
        vm.closeRequested.observe(viewLifecycleOwner) {
            dismiss()
            if (it) activityVM.dataChanged.sendAction(Unit)
        }
        vm.loadingState.observe(viewLifecycleOwner) {
            when (it) {
                NetworkState.NoData,
                is NetworkState.Error,
                NetworkState.Loaded -> binding.progress.progress.isGone = true
                NetworkState.Loading -> binding.progress.progress.isVisible = true
            }
        }
    }

    private fun submitForm() {
        var valid = true
        if (binding.name.text.isNullOrBlank()) {
            binding.tilName.error = getString(R.string.mandatory)
            valid = false
        }
        if (binding.place.text.isNullOrBlank()) {
            binding.tilPlace.error = getString(R.string.mandatory)
            valid = false
        }
        if (binding.duration.text.isNullOrBlank()) {
            binding.tilDuration.error = getString(R.string.mandatory)
            valid = false
        }
        val entryInputData = EntryCreateUseCase.EntryInputData(
            name = binding.name.text.toString(),
            place = binding.place.text.toString(),
            duration = binding.duration.text.toString(),
            timestamp = binding.timestamp.text.toString(),
            coordinates = binding.coordinates.text.toString(),
            source = when {
                binding.rbRemote.isChecked -> EntrySource.REMOTE
                else -> EntrySource.DATABASE
            },
        )
        if (valid) vm.loadEntryDetail(entryInputData)
    }

    private fun EditText.addErrorClearingTextChanged(textInputLayout: TextInputLayout): TextWatcher {
        val textWatcher = object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                // nothing
            }

            override fun beforeTextChanged(
                text: CharSequence?,
                start: Int,
                count: Int,
                after: Int
            ) {
                // nothing
            }

            override fun onTextChanged(text: CharSequence?, start: Int, before: Int, count: Int) {
                textInputLayout.error = null
            }
        }
        addTextChangedListener(textWatcher)
        return textWatcher
    }
}