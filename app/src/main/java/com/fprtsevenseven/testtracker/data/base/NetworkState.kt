package com.fprtsevenseven.testtracker.data.base

sealed class NetworkState {
    object Loading : NetworkState()
    object Loaded : NetworkState()
    class Error(val code: Int = 0) : NetworkState()
    object NoData : NetworkState()
}
