package com.fprtsevenseven.testtracker.data.entries.repositories

import com.apollographql.apollo.coroutines.await
import com.fprtsevenseven.testtracker.business.entries.face.EntryDetailFace
import com.fprtsevenseven.testtracker.business.entries.face.EntryFace
import com.fprtsevenseven.testtracker.data.EntrySource
import com.fprtsevenseven.testtracker.data.retrofit.MockApolloClient
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import queries.EntryCreateQuery
import queries.EntryDeleteQuery
import queries.EntryDetailQuery
import queries.EntryListQuery
import type.EntryInput

interface RemoteApolloRepository {

    fun getEntries(): Flow<List<EntryFace>>

    fun getEntry(face: EntryFace): Flow<EntryDetailFace?>

    fun removeEntry(face: EntryDetailFace): Flow<Boolean>

    fun createEntry(face: EntryDetailFace): Flow<Boolean>
}

/**
 * Just draft of handling GraphQL queries. Could be used instead of [RemoteRepositoryImpl],
 * if endpoints would be prepared somewhere.
 */
class RemoteApolloRepositoryImpl(
    private val apolloClient: MockApolloClient
) : RemoteApolloRepository {

    override fun getEntries(): Flow<List<EntryFace>> = flow {
        val data = apolloClient.getClient().query(EntryListQuery()).await().data
        val entries = data?.entries?.map {
            EntryFace(
                id = it.fragments.entryFragment.id,
                name = it.fragments.entryFragment.name,
                place = it.fragments.entryFragment.place,
                duration = it.fragments.entryFragment.duration,
                source = EntrySource.REMOTE,
            )
        } ?: emptyList()
        emit(entries)
    }

    override fun getEntry(face: EntryFace): Flow<EntryDetailFace?> = flow {
        val data = apolloClient.getClient().query(EntryDetailQuery(face.id)).await().data
        emit(
            data?.entry?.let {
                EntryDetailFace(
                    entryFace = EntryFace(
                        id = it.entry.fragments.entryFragment.id,
                        name = it.entry.fragments.entryFragment.name,
                        place = it.entry.fragments.entryFragment.place,
                        duration = it.entry.fragments.entryFragment.duration,
                        source = EntrySource.REMOTE,
                    ),
                    timestamp = it.timestamp,
                    coordinates = it.coordinates,
                )
            }
        )
    }

    override fun removeEntry(face: EntryDetailFace): Flow<Boolean> = flow {
        val data = apolloClient.getClient().query(EntryDeleteQuery(face.entryFace.id)).await().data
        emit(data?.success ?: false)
    }

    override fun createEntry(face: EntryDetailFace): Flow<Boolean> = flow {
        val input = EntryInput(
            name = face.entryFace.name,
            place = face.entryFace.place,
            duration = face.entryFace.duration,
            timestamp = face.timestamp,
            coordinates = face.coordinates,
        )
        val data = apolloClient.getClient().query(EntryCreateQuery(input)).await().data
        emit(data?.entry != null)
    }
}
