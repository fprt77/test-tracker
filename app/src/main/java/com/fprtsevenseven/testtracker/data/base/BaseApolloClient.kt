package com.fprtsevenseven.testtracker.data.base

import com.apollographql.apollo.ApolloClient
import okhttp3.OkHttpClient

abstract class BaseApolloClient(
    private val okHttpClient: OkHttpClient,
) {
    // provide base url according to
    abstract val baseUrl: String
    private var apolloClient: ApolloClient? = null

    fun getApollo(): ApolloClient = baseUrl.let {
        apolloClient ?: ApolloClient.builder()
            .serverUrl(it)
            .okHttpClient(okHttpClient)
            .build()
    }.also { apolloClient = it }
}

