package com.fprtsevenseven.testtracker.data.entries.di

import com.fprtsevenseven.testtracker.data.entries.repositories.*
import com.fprtsevenseven.testtracker.data.retrofit.MockApolloClient
import com.fprtsevenseven.testtracker.data.retrofit.MockRetrofitClient
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@InstallIn(SingletonComponent::class)
@Module
object RepositoryModule {

    @Singleton
    @Provides
    fun databaseRepository(): DatabaseRepository = DatabaseRepositoryImpl()

    @Singleton
    @Provides
    fun remoteRepository(
        mockRetrofitClient: MockRetrofitClient
    ): RemoteRepository = RemoteRepositoryImpl(mockRetrofitClient)

    @Singleton
    @Provides
    fun remoteApolloRepository(
        mockApolloClient: MockApolloClient
    ): RemoteApolloRepository = RemoteApolloRepositoryImpl(mockApolloClient)

}
