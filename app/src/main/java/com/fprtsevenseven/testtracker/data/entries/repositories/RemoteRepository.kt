package com.fprtsevenseven.testtracker.data.entries.repositories

import com.fprtsevenseven.testtracker.business.entries.face.EntryDetailFace
import com.fprtsevenseven.testtracker.business.entries.face.EntryFace
import com.fprtsevenseven.testtracker.data.retrofit.MockRetrofitClient
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOf

interface RemoteRepository {

    fun getEntries(): Flow<List<EntryFace>>

    fun getEntry(face: EntryFace): Flow<EntryDetailFace?>

    fun removeEntry(face: EntryDetailFace): Flow<Boolean>

    fun createEntry(face: EntryDetailFace): Flow<Boolean>
}

class RemoteRepositoryImpl(
    private val mockRetrofitClient: MockRetrofitClient
) : RemoteRepository {

    /**
     * In more later stages, this would be removed and [mockRetrofitClient] would be accessed instead.
     */
    private val inMemoryList = MockData.mockedRemoteList.toMutableList()

    override fun getEntries(): Flow<List<EntryFace>> = flow {
        emit(inMemoryList)
    }

    override fun getEntry(face: EntryFace): Flow<EntryDetailFace?> = flowOf(
        EntryDetailFace(
            entryFace = face,
            timestamp = "2021-01-01",
            coordinates = "X=13.7961000, Y=50.6460000",
        )
    )

    override fun removeEntry(face: EntryDetailFace): Flow<Boolean> = flow {
        inMemoryList.removeAll { it.id == face.entryFace.id && it.source == face.entryFace.source }
        emit(true)
    }

    override fun createEntry(face: EntryDetailFace): Flow<Boolean> = flow {
        inMemoryList.add(face.entryFace)
        emit(true)
    }
}
