package com.fprtsevenseven.testtracker.data.retrofit

import com.apollographql.apollo.ApolloClient
import com.fprtsevenseven.testtracker.data.base.BaseApolloClient
import okhttp3.OkHttpClient

interface MockApolloClient {

    fun getClient(): ApolloClient
}

class MockApolloClientImpl(okHttpClient: OkHttpClient) :
    BaseApolloClient(okHttpClient), MockApolloClient {
    override val baseUrl: String
        get() = "random.url/graphQLApi"

    override fun getClient(): ApolloClient = getApollo()
}
