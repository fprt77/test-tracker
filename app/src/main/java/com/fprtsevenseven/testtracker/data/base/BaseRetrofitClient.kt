package com.fprtsevenseven.testtracker.data.base

import okhttp3.OkHttpClient
import retrofit2.Retrofit

abstract class BaseRetrofitClient(
    private val okHttpClient: OkHttpClient,
) {
    // provide base url according to
    abstract val baseUrl: String?
    private var retrofit: Retrofit? = null
    private val apis = HashMap<Class<*>, Any?>()

    fun <T> getApi(apiClass: Class<T>): T =
        apis[apiClass] as? T ?: getRetrofit().create(apiClass).also { apis[apiClass] = it }

    fun getRetrofit(): Retrofit = checkNotNull(
        baseUrl?.let {
            retrofit ?: Retrofit.Builder()
                .baseUrl(it)
                .client(okHttpClient)
                .build()
        }.also { retrofit = it }
    )
}
