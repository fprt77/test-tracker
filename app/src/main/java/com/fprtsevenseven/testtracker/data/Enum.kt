package com.fprtsevenseven.testtracker.data

enum class EntrySource {
    DATABASE, REMOTE
}