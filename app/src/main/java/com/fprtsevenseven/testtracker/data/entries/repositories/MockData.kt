package com.fprtsevenseven.testtracker.data.entries.repositories

import com.fprtsevenseven.testtracker.business.entries.face.EntryFace
import com.fprtsevenseven.testtracker.data.EntrySource

object MockData {

    val mockedDatabaseList = listOf(
        EntryFace(
            id = "1",
            name = "Name 1",
            place = "Place 1",
            duration = "1h",
            source = EntrySource.DATABASE
        ),
        EntryFace(
            id = "2",
            name = "Name 2",
            place = "Place 2",
            duration = "2h",
            source = EntrySource.DATABASE
        ),
        EntryFace(
            id = "3",
            name = "Name 3",
            place = "Place 3",
            duration = "3h",
            source = EntrySource.DATABASE
        ),
    )

    val mockedRemoteList = listOf(
        EntryFace(
            id = "1",
            name = "Name 1",
            place = "Place 1",
            duration = "1h",
            source = EntrySource.REMOTE,
        ),
        EntryFace(
            id = "2",
            name = "Name 2",
            place = "Place 2",
            duration = "2h",
            source = EntrySource.REMOTE,
        ),
        EntryFace(
            id = "3",
            name = "Name 3",
            place = "Place 3",
            duration = "3h",
            source = EntrySource.REMOTE,
        ),
    )

}