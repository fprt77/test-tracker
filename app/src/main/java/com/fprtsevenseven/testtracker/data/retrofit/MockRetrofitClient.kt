package com.fprtsevenseven.testtracker.data.retrofit

import com.fprtsevenseven.testtracker.data.base.BaseRetrofitClient
import okhttp3.OkHttpClient
import retrofit2.Retrofit

class MockRetrofitClientImpl(
    okHttpClient: OkHttpClient,
) : BaseRetrofitClient(okHttpClient), MockRetrofitClient {

    override val baseUrl: String?
        get() = "random.url/api"
}

interface MockRetrofitClient {
    fun getRetrofit(): Retrofit
}
