package com.fprtsevenseven.testtracker.data.di

import android.content.Context
import com.fprtsevenseven.testtracker.data.retrofit.MockApolloClient
import com.fprtsevenseven.testtracker.data.retrofit.MockApolloClientImpl
import com.fprtsevenseven.testtracker.data.retrofit.MockRetrofitClient
import com.fprtsevenseven.testtracker.data.retrofit.MockRetrofitClientImpl
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

@InstallIn(SingletonComponent::class)
@Module
object BaseModule {

    @Provides
    fun getOkHttpClient(@ApplicationContext appContext: Context): OkHttpClient =
        OkHttpClient.Builder().connectTimeout(
            20,
            TimeUnit.SECONDS
        ).apply {
            addInterceptor(HttpLoggingInterceptor().apply { setLevel(HttpLoggingInterceptor.Level.BODY) })
        }.build() // you can parametrize ok http client here

    @Singleton
    @Provides
    fun gdsRetrofit(okHttp: OkHttpClient): MockRetrofitClient = MockRetrofitClientImpl(okHttp)

    @Singleton
    @Provides
    fun gdsApollo(okHttp: OkHttpClient): MockApolloClient = MockApolloClientImpl(okHttp)

}
